# [0.5.0](https://gitlab.com/soul-codes/throttle-bracket/compare/0.4.0...0.5.0) (2020-10-11)


### Features

* 🎸 later() throttler modifier ([9cd11a0](https://gitlab.com/soul-codes/throttle-bracket/commit/9cd11a0fbfc0180a2dda85610267270fd11d7b70))

# 0.4.0 (2020-10-11)

