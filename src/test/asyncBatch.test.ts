import { async } from "@lib";

test("call throttled functions only once with last argument set", async () => {
  const throttle = async(() => {
    /* not handling flush errors */
  });
  const fn = jest.fn((a: number, b: string) => void 0);
  const throttled = throttle(fn);

  let index = 0;
  throttled(++index, "hello");
  throttled(++index, "goodbye");
  expect(fn).not.toHaveBeenCalled();

  await Promise.resolve();
  expect(fn).toHaveBeenCalledTimes(1);
  expect(fn).toHaveBeenLastCalledWith(2, "goodbye");
});

test("if throttled function calls other throttled functions, they too are batched.", async () => {
  const throttle = async(() => {
    /* not handling flush errors */
  });
  const fn1 = jest.fn((a: number, b: string) => void 0);
  const throttled1 = throttle(fn1);

  let index = 0;
  const fn2 = throttle(() => {
    throttled1(++index, "hello");
    throttled1(++index, "goodbye");
  });

  fn2();
  fn2();
  expect(fn1).not.toHaveBeenCalled();

  await Promise.resolve();
  expect(fn1).toHaveBeenCalledTimes(1);
  expect(fn1).toHaveBeenLastCalledWith(2, "goodbye");
});

test("errors are caught and can be recovered from.", async () => {
  const unhandledRejects = new Array<unknown>();
  const throttle = async((exception) => {
    unhandledRejects.push(exception);
  });
  const fn1 = jest.fn((a: number, b: string) => void 0);
  const throttled1 = throttle(fn1);

  let shouldThrow = false;
  let index = 0;
  const error = Error("async error");
  const fn2 = throttle(() => {
    throttled1(++index, "hello");
    throttled1(++index, "goodbye");
    if (shouldThrow) throw error;
  });

  shouldThrow = true;
  fn2();
  fn2();

  await Promise.resolve();
  expect(unhandledRejects).toHaveLength(1);
  expect(unhandledRejects[0]).toBe(error);

  expect(fn1).toHaveBeenCalledTimes(0);

  shouldThrow = false;

  fn2();
  fn2();
  await Promise.resolve();
  expect(fn1).toHaveBeenCalledTimes(1);
  expect(fn1).toHaveBeenLastCalledWith(4, "goodbye");
});
