import { sync } from "@lib";

test("call throttled functions only once with last argument set", () => {
  const [throttle, bracket] = sync();
  const fn = jest.fn((a: number, b: string) => void 0);
  const throttled = throttle(fn);

  let index = 0;
  bracket(() => {
    throttled(++index, "hello");
    throttled(++index, "goodbye");
    expect(fn).not.toHaveBeenCalled();
  })();
  expect(fn).toHaveBeenCalledTimes(1);
  expect(fn).toHaveBeenLastCalledWith(2, "goodbye");
});

test("if throttled function calls other throttled functions, they too are batched.", () => {
  const [throttle, bracket] = sync();
  const fn1 = jest.fn((a: number, b: string) => void 0);
  const throttled1 = throttle(fn1);

  let index = 0;
  const fn2 = throttle(() => {
    throttled1(++index, "hello");
    throttled1(++index, "goodbye");
  });

  bracket(() => {
    fn2(), fn2();
  })();
  expect(fn1).toHaveBeenCalledTimes(1);
  expect(fn1).toHaveBeenLastCalledWith(2, "goodbye");
});

test("inner brackets are ignored.", () => {
  const [throttle, bracket] = sync();
  const fn1 = jest.fn((a: number, b: string) => void 0);
  const throttled1 = throttle(fn1);

  let index = 0;
  const fn2 = throttle(
    bracket(() => {
      throttled1(++index, "hello");
      throttled1(++index, "goodbye");
    })
  );

  bracket(() => {
    fn2(), fn2();
  })();
  expect(fn1).toHaveBeenCalledTimes(1);
  expect(fn1).toHaveBeenLastCalledWith(2, "goodbye");
});

test("errors are contained to the bracket and can be recovered from.", () => {
  const [throttle, bracket] = sync();
  const fn1 = jest.fn((a: number, b: string) => void 0);
  const throttled1 = throttle(fn1);

  let shouldThrow = false;
  let index = 0;
  const fn2 = throttle(() => {
    throttled1(++index, "hello");
    throttled1(++index, "goodbye");
    if (shouldThrow) throw Error("error!");
  });

  shouldThrow = true;
  expect(
    bracket(() => {
      fn2();
      fn2();
    })
  ).toThrow();
  expect(fn1).toHaveBeenCalledTimes(0);

  shouldThrow = false;
  expect(
    bracket(() => {
      fn2();
      fn2();
    })
  ).not.toThrow();
  expect(fn1).toHaveBeenCalledTimes(1);
  expect(fn1).toHaveBeenLastCalledWith(4, "goodbye");
});
