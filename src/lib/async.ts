import { Action } from "./@types";
import { throttleSet } from "./throttleSet";

/**
 * Creates an asynchronous callback batching system. Returns a callback
 * throttler function that is only called after all of the synchronous calls
 * to all throttled functions have been called.
 */
export function async(
  flushCatch: (exception: unknown) => void
): AsyncThrottler {
  let isFlushQueued = false;
  const [flush, throttle] = throttleSet(flushCatch, () => {
    if (isFlushQueued) return;
    isFlushQueued = true;
    Promise.resolve().then(() => {
      isFlushQueued = false;
      flush();
    });
  });
  return throttle;
}

export interface AsyncThrottler {
  /**
   * Converts a callback function into a asynchronously throttled version.
   * Calling this throttled version will cause the original function to be
   * called asynchronously "immediately" and only once. If the throttled
   * callback is called with arguments, the set of arguments at the most
   * recent calls will apply.
   * @param fn
   */
  <T extends Action>(fn: T): T;

  later(): AsyncThrottler;
}
