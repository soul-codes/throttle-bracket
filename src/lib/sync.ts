import { Function } from "./@types";
import { Throttler, throttleSet } from "./throttleSet";

/**
 * Creates a synchronous transactional event batching system. Returns a tuple
 * the throttle-bracket pair.
 */
export function sync(): readonly [
  throttle: Throttler,
  bracket: TransactionBracket
] {
  const [flush, throttle] = throttleSet(
    (exception) => {
      throw exception;
    },
    () => !transactionDepth && flush()
  );

  let transactionDepth = 0;
  const bracket = ((fn) => (...args) => {
    try {
      transactionDepth++;
      return fn(...args);
    } finally {
      !--transactionDepth && flush();
    }
  }) as TransactionBracket;

  return [throttle, bracket];
}

export interface TransactionBracket {
  /**
   * Creates a version of the function that also creates a transaction context.
   * Within this transaction context, calls to the throttled callbacks will be
   * batched until outermost transaction context ends. The effect is that, if
   * this wrapped effectful function here causes multiple callbacks to be fired
   * synchronously, they will instead be deferred until the very end.
   */
  <T extends Function>(fn: T): T;
}
