import { throttleSet } from "@lib";

const [flush, throttle] = throttleSet(
  () => {
    /* exception not handled */
  },
  () => flush()
);

test("should call each function only once", () => {
  const fn1 = jest.fn(() => (t2(), t2()));
  const fn2 = jest.fn(() => (t3(), t3()));
  const fn3 = jest.fn();

  const t1 = throttle(fn1);
  const t2 = throttle(fn2);
  const t3 = throttle(fn3);

  t1();
  expect(fn1).toHaveBeenCalledTimes(1);
  expect(fn2).toHaveBeenCalledTimes(1);
  expect(fn3).toHaveBeenCalledTimes(1);
});

test("should call all functions of throttle level n before level n + 1", () => {
  const fn1 = jest.fn(() => (t2(), t3(), t4()));
  const fn2 = jest.fn(() => (t3(), t4()));
  const fn3 = jest.fn();
  const fn4 = jest.fn();

  const t1 = throttle(fn1);
  const t2 = throttle(fn2);
  const t3 = throttle.later()(fn3);
  const t4 = throttle.later().later()(fn4);

  t1();
  expect(fn1).toHaveBeenCalledTimes(1);
  expect(fn2).toHaveBeenCalledTimes(1);
  expect(fn3).toHaveBeenCalledTimes(1);
  expect(fn4).toHaveBeenCalledTimes(1);

  expect(fn1).toHaveBeenCalledBefore(fn3);
  expect(fn2).toHaveBeenCalledBefore(fn3);
  expect(fn1).toHaveBeenCalledBefore(fn4);
  expect(fn2).toHaveBeenCalledBefore(fn4);
  expect(fn3).toHaveBeenCalledBefore(fn4);
});
