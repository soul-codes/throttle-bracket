export type Function = (...args: any[]) => any;
export type Action = (...args: any[]) => void;
