import { async } from "./async";
import { sync } from "./sync";

export const [throttle, bracket] = sync();
export const throttleAsync = async((exception) => {
  console.error(exception);
});
