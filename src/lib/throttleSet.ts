import { Action } from "./@types";
import { RECURSION_TOLERANCE } from "./RECURSION_TOLERANCE";

export function throttleSet(
  flushCatchFn: (exception: unknown) => void,
  requestFlush: () => void
): readonly [flush: () => void, throttle: Throttler] {
  let levelBuffers = new Array<Map<Action, any[]> | null>();
  let isFlushing = false;
  let flushLevel = 0;

  function flush() {
    if (isFlushing) return;
    let recursions = 0;
    try {
      outer: while (true) {
        for (let i = flushLevel; i < levelBuffers.length; i++) {
          while (true) {
            const buffer = levelBuffers[i];
            levelBuffers[i] = null;
            if (!buffer?.size) break;

            flushLevel = i;
            const currentBuffer = buffer;
            if (recursions++ > RECURSION_TOLERANCE) {
              throw Error(
                `More than ${RECURSION_TOLERANCE} cycles of batch flushings have occurred. This likely indicates that there is a throttled function that indefinitely calls itself.`
              );
            }

            for (const [cb, args] of currentBuffer) {
              try {
                isFlushing = true;
                cb(...args);
              } finally {
                isFlushing = false;
              }
            }
          }
          if (flushLevel < i) continue outer;
        }
        break;
      }
    } catch (exception) {
      flushCatchFn(exception);
    } finally {
      levelBuffers = [];
      flushLevel = 0;
    }
  }

  const createThrottler = (level: number): Throttler =>
    Object.assign(
      ((fn) => (...args: any[]) => {
        const buffer = levelBuffers[level] || (levelBuffers[level] = new Map());
        buffer.set(fn, args);
        flushLevel = Math.min(flushLevel, level);
        if (!isFlushing) requestFlush();
      }) as Throttler,
      { later: () => createThrottler(level + 1) } as Throttler
    );

  return [flush, createThrottler(0)];
}

export interface Throttler {
  /**
   * Converts a callback function into a asynchronously throttled version.
   * Calling this throttled version will cause the original function to be
   * called asynchronously "immediately" and only once. If the throttled
   * callback is called with arguments, the set of arguments at the most
   * recent calls will apply.
   * @param fn
   */
  <T extends Action>(fn: T): T;

  later(): Throttler;
}
